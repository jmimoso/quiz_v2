module.exports = (env) => ({
	"Intl_fallback_language"  : "en",
	"Intl_default_language"   : "pt-PT",
	"Intl_supported_languages": ["en", "pt-PT"],
	"Intl_default_namespace"  : "core"
});
import BabelPolyfill from 'babel-polyfill';
import 'raf/polyfill';
import 'whatwg-fetch';

export default () => ({
	BabelPolyfill,
	Raf
});
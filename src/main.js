import styles from 'root/main.scss';

import { isIE } from 'helpers/Utils';
import { ACTIONS } from 'common/Constants';
import StoreFactory from 'factories/StoreFactory';

const initStores = () => {
	StoreFactory.create('SimpleStore',
		require('stores/SimpleStore').default,
		require('reducers/SimpleReducer').default,
		ACTIONS['SIMPLE_TEXT']);
};

const init = () => {
	const React = require('react');
	const ReactDOM = require('react-dom');

	const TranslationsHandler = require('handlers/TranslationsHandler').default;
	const App = require('views/App').default;

	let Router = {};

	if (process.env.NODE_ENV !== 'development') {
		Router = require('react-router-dom/HashRouter').default;
	}; 
	
	if (process.env.NODE_ENV === 'development') {
		Router = require('react-router-dom/BrowserRouter').default;
	};

	initStores();

	ReactDOM.render(
		(<Router>
			<App />
		</Router>),
		document.getElementById('root')
	);
};

if (!isIE()) {
	init();
} else {
	require('babel-runtime/core-js/promise').default = require('es6-promise-promise');
	import(/* webpackChunkName: "mainIE" */ 'root/mainIE.js').then(() => init());
};
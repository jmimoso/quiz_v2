import React, { Component } from 'react';
import TranslationsHandler from 'handlers/TranslationsHandler';
import Utils from 'helpers/Utils';
import Actions from 'common/Actions';

import Provider from 'components/Provider';

class OtherRouteController extends Component {
	constructor(props) {
		super(props);

		this.state = this.buildState();
		return this;
	}

	componentWillReceiveProps(nextProps) {
		this.setState(this.buildState(nextProps));
		return this;
	}

	buildState(nextProps) {
		const props = nextProps || this.props;

		return {
			text: props.dataset.text
		};
	}

	handleChange({ target }) {
		Actions.SimpleStoreUpdate({text: target.value});
		//this.setState({ text: target.value})
		return this;
	}

	render() {
		const { text } = this.state;
		
		//eg: store use
		return (
			<div className="other-route-container">
				<input 
					type="text"
					value={text}
					placeholder={TranslationsHandler.translate('Write')}
					onChange={evt=>this.handleChange(evt)}
				/>
				<span>{text}</span>
			</div>
		);
	}
};


export default Provider(OtherRouteController, ['SimpleStore'], ({ SimpleStore }) => ({
	text: SimpleStore.getText()
}));
import React, { Component } from 'react';
import Utils from 'helpers/Utils';

import View from './EntryView';

class EntryController extends Component {
	constructor(props) {
		super(props);

		this.state = this.buildState();
		return this;
	}
	componentDidMount() {
		return this;
	}

	componentWillUnmount() {
		return this;
	}

	buildState(nextProps) {
		return {
		};
	}

	render() {
		return (
			<View {...this.props} {...this.state} />
		);
	}
};

export default EntryController;
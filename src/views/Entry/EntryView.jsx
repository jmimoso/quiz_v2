import React from 'react';

import Translation from 'components/Translation';

export default props => (
	<h1 className="hello-world">
		<Translation tkey='Hello world!' />
	</h1>
);
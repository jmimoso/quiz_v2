import React, { Component } from 'react';
import Route from 'react-router-dom/Route';
import Link from 'react-router-dom/Link';


import Utils from 'helpers/Utils';

import Translation from 'components/Translation';
import Entry from 'views/Entry';
import OtherRoute from 'views/OtherRoute';

class AppController extends Component {
	constructor(props) {
		super(props);

		this.state = this.buildState();
		return this;
	}
	componentDidMount() {
		return this;
	}

	componentWillUnmount() {
		return this;
	}

	buildState(nextProps) {
		return {
		};
	}

	render() {

		return (
			<div className='main-container'>
				<Link to='/'> <Translation tkey='Entry' /> </ Link>
				<Link to='/other-route'> <Translation tkey='Other route' /> </ Link>

				<br/>
				<Route
					exact
					path={'/'}
					component={Entry}
				/>
				<Route
					exact
					path={'/other-route'}
					component={OtherRoute}
				/>
			</div>
		);
	} 
};

export default AppController;
import React, { Component } from 'react';
import StoresCollection from 'collections/StoresCollection';
import { obj } from 'helpers/DataTypesHelper';
import Utils from 'helpers/Utils';
import Log from 'handlers/DebugHandler';

const createStateHash = state => obj.JSONStringifySafely(state);

const Provider = (WrappedComponent, dataSources, selectData) => {
	return class ProviderWrapper extends Component {
		constructor(props) {
			super(props);

			this._stores = this.retrieveDataSource();
			this._uuid = Utils.generateUUID();
			this.state = this.buildState();
			return this;
		}

		/**
		 * Retrieves the stores that the component needed
		 * @returns {Object} Instace of this class
		 */
		retrieveDataSource() {
			return obj.extractPropertiesSafely(dataSources, 
				StoresCollection.storedData());
		}

		buildState(nextProps) {
			return {
				dataset: selectData(this._stores, (nextProps || this.props))
			}
		}

		componentDidMount() {
			for (let key in this._stores) {
				this._stores[key].bind(this, this.handleChange);
			};
			return this;
		}
		 
		componentWillUnmount() {
			for (let key in this._stores) {
				this._stores[key].unbind(this);
			};
			
			delete this._uuid;
			delete this._stores;
			return this;
		}

		componentWillReceiveProps(nextProps) {
			return this.updateState(nextProps);
		}

		handleChange(storeNewState) {
			return this.updateState();
		}

		updateState(nextProps) {
			this.setState(this.buildState(nextProps));
			return this;
		}

		render() {
			return <WrappedComponent { ...this.state } { ...this.props } />;
		}
	};
};

export default Provider;
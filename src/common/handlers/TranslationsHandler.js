import React from 'react';
import i18n from 'i18next';
import Log from 'handlers/DebugHandler';
import { array } from 'helpers/DataTypesHelper';
import SubscriptionBaseClass from 'mixins/SubscriptionBaseClass';

async function initLib(configs) {
	return new Promise((resolve, reject) => i18n.init(configs, () => resolve(i18n)));
};

async function libChangeLanguage(lng) {
	return new Promise((resolve, reject) => i18n.changeLanguage(lng, () => resolve(i18n)));
};

const addLoadedNamespace = (scope, namespace, language) => {
	
	scope._loadedNS = array.pushIfNotExist(scope._loadedNS, namespace);
	scope._mapLangLoadedNS[language] = array.pushIfNotExist(scope._mapLangLoadedNS[language], namespace);
	
	Log.debug(`%c TRANSLATIONS ARE READY :)`, 'background: #8bc34a; color: #fff',
		`\n -> namespace: ${namespace} - language: ${language}`);

	return scope._loadedNS;
};

const changeLanguageIfSupported = (scope, lng, resolve, reject) => {
	if (!scope.lngIsSupported(lng)) {
		return reject(new Error('Language is not supported'));
	};

	scope._selectedLng = lng;

	return resolve(scope.loadingLngForAlreadyLoadedNs(lng));
};

const preventLoadingCachedFiles = (cachedFilesMap, lng, ns) => {
	const cachedLng = cachedFilesMap[lng];

	if (cachedLng && array.inArray(cachedLng, (item) => item === ns)) {
		return new Promise((resolve, reject) => resolve());
	};

	return import(/* webpackChunkName: "translations." */ `locales/${lng}/${ns}`);
};

class TranslationsHandler extends SubscriptionBaseClass {
	constructor(options) {
		super(options);

		this._ready 				= false;
		this._defaultNS             = __DEFINITIONS__.Intl_default_namespace;
		this._selectedLng           = __DEFINITIONS__.Intl_default_language;
		this._selectedNS            = this.getDefaultNS();
		this._loadedNS              = [];
		this._mapLangLoadedNS       = {};
		this._subscribersStorageKey = 'subscribe';
		
		this.initTranslations();
		return this;
	}

	bind(scope, callback) {
		this.subscribe(scope, this._subscribersStorageKey, callback);
		return this;
	}

	unbind(scope) {
		this.unsubscribe(scope, this._subscribersStorageKey);
		return this;
	}
	
	async initTranslations() {
		return initLib( this.getDefaultConfigs() )
			.then(i18n => this.loadTranslationFileSync())
			.then(translation =>  {
				addLoadedNamespace(this, this.getDefaultNS(), this.getSelectedLng());
				this._ready = true;
				return this.addTranslationFile(translation)
			})
			.then(translation => this.emitChange(this._subscribersStorageKey, () => translation));
	}
	
	getDefaultConfigs() {
		return {
			fallbackLng  : __DEFINITIONS__.Intl_fallback_language,
			whitelist    : this.getSupportedLngs(),
			lng          : this.getSelectedLng(),
			ns           : [this.getDefaultNS()],
			defaultNS    : this.getDefaultNS(),
			fallbackNS 	 : [this.getDefaultNS()],
			load         : 'currentOnly',
			initImmediate: false,
			returnObjects: true,
			debug        : false
		}
	}

	loadTranslationFileSync() {
		return require('locales/<!-- @defaultLng Intl_default_language -->/<!-- @defaultLng Intl_default_namespace -->').default;
	}

	async loadTranslationFileAsync(lng = this.getSelectedLng(), ns = this.getSelectedNS()) {

		return new Promise((resolve, reject) => {
			return preventLoadingCachedFiles(this._mapLangLoadedNS, lng, ns, resolve, reject)
				.then(translation => addLoadedNamespace(this, ns, lng) && translation && translation.default)
				.then(translation => this.addTranslationFile(translation))
				.then(translation => resolve(translation))
				.catch(error => {
					Log.error(error);
					return reject(error);
				})
		});
	}

	async loadingLngForAlreadyLoadedNs(lng) {
		const namespaces = this._loadedNS;
		let index        = 0;
		let length       = namespaces.length;
		let ns           = '';
		let translations = [];

		for (index; index < length; index++) {
			
			ns = namespaces[index];
			translations.push(await this.loadTranslationFileAsync(lng, ns));
		};

		return translations;
	}

	async changeLanguage(lng) {

		
		return new Promise((resolve, reject) => {
			if (!this.isReady()) {
				
				return setTimeout(() => {
					return changeLanguageIfSupported(this, lng, resolve, reject);
				}, 100);
			};

			return changeLanguageIfSupported(this, lng, resolve, reject)
		})
		.then(() => libChangeLanguage(this.getSelectedLng()))
		.then(() => this.getTranslations())
		.then(translation => this.emitChange(this._subscribersStorageKey, () => translation));
	}

	async changeNamespace(namespace) {

		return this.loadTranslationFileAsync(undefined, namespace)
			.then(() => (this._selectedNS = namespace))
			.then(() => i18n.setDefaultNamespace(this.getSelectedNS()));
	}

	addTranslationFile(resource) {
		if (resource) {
			i18n.addResourceBundle(this.getSelectedLng(), this.getDefaultNS(), resource(React));
		};
		return resource;
	}

	lngIsSupported(lng) {
		return (this.getSupportedLngs().indexOf(lng) > -1);
	}

	getSelectedLng() {
		return this._selectedLng;
	}

	getSelectedNS() {
		return this._selectedNS;
	}

	getDefaultNS() {
		return this._defaultNS;
	}

	getSupportedLngs() {
		return __DEFINITIONS__.Intl_supported_languages;
	}

	getTranslations() {
		return i18n.getResourceBundle(this.getSelectedLng(), this.getSelectedNS())
	}

	isReady() {
		return this._ready;
	}

	translate(tkey) {
		return i18n.t(tkey);
	}
};

export default new TranslationsHandler();
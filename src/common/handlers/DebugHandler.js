import { obj } from 'helpers/DataTypesHelper';
import Constants from 'common/Constants';

class DebugHandler {

	/**
	 * Creates an instance of DebugHandler.
	 */
	constructor() {
        // Don't change this line, webpack can't handle variables. eg: 
        // const env = process.env.NODE_ENV;
        // if(env !== Constants.ENVIRONMENT.TYPE.PRODUCTION) {
        //    ...    
        // This way the module is added to the bundle, and we get code that is not used.
        if(process.env.NODE_ENV !== 'production') {

            this._onlyDev = 'This line should not be in the production build';

            try{
                const log      = require('loglevel');
                const ENV_TYPE = Constants.ENVIRONMENT.TYPE;

                switch (process.env.NODE_ENV.trim()) {
                    case ENV_TYPE.DEVELOPMENT:
                    case ENV_TYPE.DISTRIBUTION:
                        log.enableAll(); /* TODO - read def_dev to set a custom log.getLevel() to core logs */
                        break;
                    case ENV_TYPE.TEST:
                        return this;
                    default:
                        break;
                };

                return obj.assign(this, log);
            } catch(error) {}
        };
	}
};

/**
 * Prints a stack of calls that the log will produce
 * @param {any} args
 */
DebugHandler.prototype.trace = function (args) { };

/**
 * Default log
 * @param {any} args
 */
DebugHandler.prototype.debug = function (args) { };

/**
 * Adds a info icon before the log
 * @param {any} args
 */
DebugHandler.prototype.info = function (args) { };

/**
 * Adds a warning icon before the log. 
 * Should be used on situations that the code will not break, but its occurrence is unexpected or not desired.
 * @param {any} args
 */
DebugHandler.prototype.warn = function (args) { };

/**
     * Adds a error icon before the log, also prints the calling stack. 
     * Should be used on situations that the code will break.
     * @param {any} args
     */
DebugHandler.prototype.error = function (args) {
    console.error ? console.error.apply(null, arguments) : console.log.apply(null, arguments);
};

const debugHandler = new DebugHandler();
const { trace, debug, info, warn, error } = debugHandler;

export default debugHandler;
export { trace, debug, info, warn, error };
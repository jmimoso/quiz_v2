import Store from 'stores/base/BaseStore';
import { obj } from 'helpers/DataTypesHelper';

class StoresCollection extends Store{
	constructor(props) {
		super(props);
		return this;
	}

	buildInitState() {
		return {};
	}

	addStore(id, storeInstance) {
		let data = {};
		data[id] = storeInstance;
		
		this.storeData(obj.assign(
			this.storedData(), data));
		return this;
	}

	getById(id) {
		return this.storedData()[id];
	}
};

export default new StoresCollection();
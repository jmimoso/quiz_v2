import Log from 'handlers/DebugHandler';
import Utils from 'helpers/Utils';

class Subscription {
	constructor(props) {
		this._defaultEvent = 'subscribe';
		return this;
	}

	getEventStorageName(evt = this._defaultEvent) {
		const event = '_'.concat(evt);
		return event;
	}

	initEvent(originalEvtType, proxyCb = null) {
		return {
			proxy: proxyCb,
			subscribers: {}
		};
	}

	initSubscriber(scope) {
		return { scope, cbs: [] };
	}

	getStoredEventByType(type) {
		const event = this.getEventStorageName(type);
		return this[event];
	}

	subscribe(scope = {}, type, callback = (() => null), proxyCb) {
		const event = this.getEventStorageName(type);
		const id = scope._EventUUID || Utils.generateUUID();

		this[event] = this[event] || this.initEvent(type, proxyCb);
		this[event].subscribers[id] = this[event].subscribers[id] || this.initSubscriber(scope);
		scope._EventUUID = id;

		this[event].subscribers[id].cbs.push(callback);
		return this;
	}

	unsubscribe(scope = {}, type) {
		const event = this.getEventStorageName(type);
		const id = scope._EventUUID;
		let subscribers = [];
		
		if (!id || !this[event] || !this[event].subscribers[id]) {
			return this;
		};
		
		this[event].subscribers[id].cbs = [];
		delete this[event].subscribers[id];
		
		subscribers = Object.keys(this[event].subscribers);

		if (subscribers.length > 0) {
			return this;
		};

		this[event].proxy = null;
		delete this[event];

		return this;
	}

	emitChange(type, dataSource = (() => null)) {
		const event = this.getEventStorageName(type);
		
		if (!this[event]) {
			return this;
		};

		const subscribers = this[event].subscribers;
		let subscriber = {};

		for (let key in subscribers) {
			subscriber = subscribers[key];
			subscriber.cbs.map(
				fn => fn.call(subscriber.scope, dataSource.call(this)));
		};

		return this;
	}
}

export default Subscription;
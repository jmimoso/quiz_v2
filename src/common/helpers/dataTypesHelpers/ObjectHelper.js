import objectAssign from 'object-assign';
import Log from '../../handlers/DebugHandler';

/**
 * Merges all given parameters
 * 
 * @param {Object} args, list of objects
 * @returns
 */
export const assign = function(args) { 
	return objectAssign.apply(null, arguments);
};

/**
 * Extracts a list of properties from a given object a returns a new object that contains only those properties
 * 
 * @param {array} propertiesList, list of properties
 * @param {object} source, object that contains the properties
 * @returns new object
 */
export const extractPropertiesSafely = (propertiesList, source) => {
	if(!propertiesList || !propertiesList.length || !source) return {};

	let safeProperties = {};

	propertiesList.map((property) => {
		if(source[property] !== undefined) safeProperties[property] = source[property];
	});
	return safeProperties;
};

/**
 * Converts a JavaScript value to a JSON string, however this implementation prevents errors of circular dependencies
 * 
 * @param {bject} json, the value to convert to a JSON string.
 * @returns {string} JSON string
 */
export const JSONStringifySafely = (json) => {
	let cache = [];
	let string = '';

	try {
		string = JSON.stringify(json);
	} catch (error) {
		
		Log.error(error);

		string = JSON.stringify(json, (key, value) => {
			
			if (typeof value === 'object' && value !== null) {
				if (cache.indexOf(value) !== -1) {
					// Circular reference found, discard key
					return;
				};
				// Store value in our collection
				cache.push(value);
			}
	
			return value;
		});
	};

	cache = [];

	return string;
};

export default{
	assign,
	extractPropertiesSafely,
	JSONStringifySafely
};
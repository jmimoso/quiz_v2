export const ENVIRONMENT = {
	TYPE: {
		DEVELOPMENT: 'development',
		DISTRIBUTION: 'distribution',
		PRODUCTION: 'production',
		TEST: 'test'
	}
};

export const ACTIONS = {
	'SIMPLE_TEXT': {
		'UPDATE': 'UPDATE'
	}
};

export default {
	ENVIRONMENT,
	ACTIONS
};
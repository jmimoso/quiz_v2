import Log from 'handlers/DebugHandler';
import StoresCollection from 'collections/StoresCollection';
import { str } from 'helpers/DataTypesHelper';
import Actions from 'common/Actions';

const createActions = (storeNamespace, actions, reducer, store) => {

	actions && Object.keys(actions).map((action) => {
		const fnName = ''.concat(storeNamespace, str.toCamelCase(action, '_', true));
		
		Log.debug("%c NEW Action: ", 'background: #90d9ee; color: #222; font-weight: bold;',  fnName)
		
		Actions[fnName] = (payload) => {
			return store.storeData(
				reducer(store.storedData(), {type: action, data: payload}, store))
					.storedData();
		};
	});
};

class StoreFactory {
	constructor(props) {
		return this;
	}

	buildStoredData(newData) {
		return newData || [];
	}

	create(namespace, StoreClass, reducer, actions) {
		const store = new StoreClass();

		StoresCollection.addStore(namespace, store);
		createActions(namespace, actions, reducer, store)
		return store;
	}
};

export default new StoreFactory();